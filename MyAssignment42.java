/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment42;

/**
 *
 * @author user
 */
public class Assignment42 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to generate triangle using digits.
        int count = 0;
        for(int i=1; i<=4; i++){
            for(int j=1; j<=i; j++){
                count++; 
                System.out.print(count+" ");
            }
            System.out.println("");
        }
    }   
}
