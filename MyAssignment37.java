/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment37;

/**
 *
 * @author user
 */
import java.util.Scanner;
public class Assignment37 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to accept 3 digits number and print the digits in reverse.
        Scanner sc = new Scanner(System.in);
        String newN = "";
          System.out.print("Enter a 3 digits number: ");
          String N = sc.next();
          int length = N.length();
          for(int i=length-1; i>=0; i--){
              newN += N.charAt(i);
          }
              System.out.println("The number in reverse is: "+newN);
    }
}