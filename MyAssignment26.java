/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment26;

/**
 *
 * @author user
 */
public class Assignment26 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to display the first 10 nutural numbers.
        System.out.println("The first 10 natural numbers are: ");
        for(int num = 1; num <= 10; num++)
            System.out.print(num+", ");   
    }    
}
