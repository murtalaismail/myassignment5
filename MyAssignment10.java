/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment10;

/**
 *
 * @author MURTALA
 */
public class Assignment10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // This program swaps 2 numbers without using a 3rd variable.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter the first number: ");
        int n1 = sc.nextInt();
        System.out.print("Enter the second number: ");
        int n2 = sc.nextInt();
        
        if(n1==n2 || (n1==0 && n2==0)){  
            System.out.print("The numbers needn't to be swapped."+"\n");
            return;
        }
        
        System.out.println("The value of n1 before swap is: "+n1);
        System.out.println("The value of n2 before swap is: "+n2);
        System.out.println();
        
        n1=n1+n2;
        n2=n1-n2;
        n1=n1-n2;
        
        System.out.println("The value of n1 after swap is: "+n1);
        System.out.println("The value of n2 after swap is: "+n2);           
    }
}