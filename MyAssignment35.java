/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment35;

/**
 *
 * @author user
 */
public class Assignment35 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to generate Fibonacci series of up to n numbers.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter a number n to generate its Fibonacci: "); 
        int f1=1,f2=1,f3=0;
        int num = sc.nextInt();
        if(num == 0 || num == 1)
            System.exit(0);
        
        System.out.print(f1+","+f2);
        for(int i = 1; i <= num; i++){
            f3 = f1+f2;
            System.out.print(","+f3);
            f1 = f2;
            f2 = f3;  
        }
    }
}