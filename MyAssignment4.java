/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment4;
/**
 *
 * @author MURTALA
 */
public class Assignment4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Program that calculates the area of Circle, Square and Rectangle.
        java.util.Scanner i = new java.util.Scanner(System.in);
        System.out.println("Enter the radius: ");
        int r = i.nextInt();
        System.out.print("Enter the length");
        int l = i.nextInt();
        System.out.print("Enter the breadth");
        int b = i.nextInt();
        
        double c_area = (Math.PI)*(Math.pow(r,2));
        int r_area = l*b;
        int s_area = l*l;
        
        System.out.println("The area of the circle is: "+c_area);
        System.out.println("The area of the rectangle is: "+r_area);
        System.out.println("The area of the square is: "+s_area); 
    }
}