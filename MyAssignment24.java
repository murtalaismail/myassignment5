/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment24;

/**
 *
 * @author user
 */
import java.util.Scanner;
public class Assignment24 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // program to display the use of arithmetic operators using switch case.
        Scanner sc = new Scanner(System.in);
        char opr;
        int result;
        System.out.print("Enter the first value: ");
        int x = sc.nextInt();
        System.out.print("Enter the second number: ");
        int y = sc.nextInt();
        System.out.print("Enter the operator: ");
        opr = (sc.next()).charAt(0);
        
        switch(opr){
            case '+': result = x + y; break;
            case '-': result = x - y; break;
            case '*': result = x * y; break;
            case '/': result = x / y; break;
            case '%': result = x % y; break;
            default: result: System.out.println("Invalid operator");
            return;
        }
        System.out.println(x+""+opr+""+y+" = "+result);      
    }
}