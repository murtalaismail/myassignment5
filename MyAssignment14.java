/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment14;

/**
 *
 * @author user
 */
public class Assignment14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to check whether a number is even or odd.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter the number: ");
        int num = sc.nextInt();
        
        if(num%2!=0){
            System.out.println("The number "+num+" is odd.");
        }
        else{
            System.out.println("The number "+num+" is even.");
        }
                
    }    
}
