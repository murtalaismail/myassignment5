/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

/**
 *
 * @author MURTALA
 */
public class Assignment6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to find the average between 7 numbers.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter seven numbers: ");
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        int n3 = sc.nextInt();
        int n4 = sc.nextInt();
        int n5 = sc.nextInt();
        int n6 = sc.nextInt();
        int n7 = sc.nextInt();
        int sum = n1+n2+n3+n4+n5+n6+n7;
        double average = (double)(sum)/7;
        
        System.out.println("The average of the numbers is: "+average);
    }
}