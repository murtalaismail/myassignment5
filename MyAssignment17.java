/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment17;

/**
 *
 * @author user
 */
public class Assignment17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to check whether a year is leaf year or not.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter the year to check if is leaf year or not: ");
        int year = sc.nextInt();
        
        if(year%4 == 0){
            System.out.println("The year is a leaf year.");
        }
        else{
            System.out.println("The year is not leaf year.");
        }
    }
}