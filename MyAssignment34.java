/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment34;

/**
 *
 * @author user
 */
public class Assignment34 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // To print even numbers out of first 100 numbers.
        int n  = 1;
        while(n<=100){
            if(n%2 != 0)
                ++n;
            System.out.print(n+",");
        n++;
        }
    } 
}
