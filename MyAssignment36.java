/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment36;

/**
 *
 * @author user
 */
public class Assignment36 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // This program generates multiplication table of a number.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter a number to print its table: ");
       int num = sc.nextInt();
        for(int i=1; i<=num; i++){
            for(int j=2; j<=num; j++){
                System.out.print(j+" * "+i+" = "+(j*i+"\t"));
            }
               System.out.println("");
        }
    }  
}
