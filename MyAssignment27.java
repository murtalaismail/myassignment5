/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment27;

/**
 *
 * @author user
 */
public class Assignment27 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to print first N natural numbers
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter the value of n: ");
        int n = sc.nextInt();
        System.out.println("");
        
        for(int i=1; i<=n; i++){
            System.out.print(i+",");
        }
    }
}
