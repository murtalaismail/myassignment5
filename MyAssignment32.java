/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment32;

/**
 *
 * @author user
 */

public class Assignment32 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* To print the squares of the first 10 natural numbers, 
           and their sum using while loop.*/
        
        int n = 1, square, sum = 0;
        while(n<=10){
            square = n*n;
            sum += square;
            System.out.println("The square of "+n+" = "+square);
            n++;
        }  
        System.out.println("The sum of the squares = "+sum);
    }
}