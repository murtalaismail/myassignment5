/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment7;

/**
 *
 * @author MURTALA
 */
public class Assignment7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to calculate powers of numbers.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter the values of a and b: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        if(a==0 && b==0)
            System.exit(0);
        
        int result1 = (int)(Math.pow((a+b), 2));
        int result2 = (int)(Math.pow((a+b), 3));
        int result3 = (int)(Math.pow(a, 2)-Math.pow(b, 2));
        
        if(result3<0){
            result3 = -result3;
        }
                 
        System.out.println("The first result is: "+result1);
        System.out.println("The second result is: "+result2);
        System.out.println("The third result is: "+result3);
    }
}