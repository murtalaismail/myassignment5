/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment18;

/**
 *
 * @author user
 */
public class Assignment18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to check the greatest among two numbers using Conditional Operators.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter the first number:");
        int num1 = sc.nextInt();
        System.out.println("Enter the second number:");
        int num2 = sc.nextInt();
        String greater = (num1>num2)?num1+" is greater than "+num2:num2+" is greater than "+num1;
        System.out.println(greater);
    }
}