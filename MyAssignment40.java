/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment40;

/**
 *
 * @author user
 */
public class Assignment40 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // To print prime numbers out of the first 100 numbers.
        int num = 100;
        
        Prime:
        for(int i=2; i<=num; i++){
            for(int j=2; j<i; j++){
                if(i%j==0){
                    continue Prime;
                }
            }
            
        System.out.println(i);
        }
    }
}