/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment29i;

/**
 *
 * @author user
 */
public class Assignment29I {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // To generate (i) 10,9,8,...1
        System.out.println("For (i) the answer is: ");
        for(int i = 10; i >= 1; i--){
            System.out.print(i+",");
        }
        System.out.println(""+"\n");
        
        //To generate (ii) 2,4,6,8,...20
        System.out.println("For (ii) the answer is: ");
        for(int i = 2; i <= 20; i+=2){
            System.out.print(i+",");
        }
        System.out.println(""+"\n");
        
        //To generate (iii) 10,13.5,17,20.5
        System.out.println("For (iii) the answer is: ");
        for(double i = 10; i<=20.5; i+=3.5)
            System.out.print(i+",");
    } 
}