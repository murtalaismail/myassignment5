/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment16;

/**
 *
 * @author user
 */
public class Assignment16 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // This program accepts the percentage marks of a student and prints whether is PASS or FAIL.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter the percentage marks of a student: ");
        int marks = sc.nextInt();
        if(marks >= 45 && marks <=100)
            System.out.println("PASS");
        else
            System.out.println("FAIL");
    }
}