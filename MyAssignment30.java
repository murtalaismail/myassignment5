/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment30;

/**
 *
 * @author user
 */
public class Assignment30 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // To calculate the factorial of a given number using for loop and while loop.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        long fact = 1;
        System.out.print("Enter the number to calculate its factorial using for loop: ");
        int n = sc.nextInt();
        for(int i = n; i >= 1; i--){
            fact *= i;
        }
        System.out.println("The factorial of the number "+n+"!"+ " = "+fact);
        System.out.println(""+"\n");
        
        // This section uses while loop to calculate facorial.
        System.out.println("(ii) Calculating the factorial using while loop");
        System.out.println("Enter the number to calculate its factorial");
        int num = sc.nextInt();
        fact = 1;
        while(n >= 1){
            fact *= n;
            n--;
        }
        System.out.println("The factorial of the number "+num+" is "+fact);
        System.out.println(""+"\n");
    }
}