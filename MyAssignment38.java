/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment38;

/**
 *
 * @author user
 */
import java.util.Scanner;
public class Assignment38 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // This program that checks if a number is palindrome.
        Scanner input = new Scanner(System.in);
        String newPalinInteger = "";
        System.out.println("Enter a set of integers to check if is palindrome");
        String PalinInteger = input.next();
        int length = PalinInteger.length();

        for(int j=length-1;j>=0;j--){
            newPalinInteger += PalinInteger.charAt(j);
        }
        if(PalinInteger.equals(newPalinInteger)){
            System.out.println("The Integer is palindrome");
        }
        else{
            System.out.println("The Integer is not a palindrome");
        }
        System.out.println("The new Integer is "+newPalinInteger);
    }
}