/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment8;

/**
 *
 * @author MURTALA
 */
public class Assignment8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to calculate the circumference of a circle.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.print("Enter the radius: ");
        int r = sc.nextInt();
        if(r==0){
            System.out.print("This is not a circle."+"\n");
            System.exit(0);
        }
        double circ = 2*Math.PI*r;
        
        System.out.println("The circumference of the circle is: "+circ);
    }
}