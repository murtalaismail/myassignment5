/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment13;

/**
 *
 * @author user
 */
public class Assignment13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to check whether a person can vote.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter the age of the person: ");
        int age = sc.nextInt();
        
        if(age>=18){
            System.out.println("The person can vote, issue ballot paper.");
        }
        else{
            System.out.println("The person is under age, hence cannot vote.");
        }
    }
}
