/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment23;

/**
 *
 * @author user
 */
public class Assignment23 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to display the days of the week using switch case.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter an integer to choose a day: ");
        int id = sc.nextInt();
        String day;
        
        switch(id){
            case 1:
                day = "Sundy"; break;
            case 2:
                day = "Monday"; break;
            case 3:
                day = "Tuesday"; break;
            case 4:
                day = "Wednesday"; break;
            case 5:
                day = "Thursday"; break;
            case 6:
                day = "Friday"; break;
            case 7:
                day = "Saturday"; break;
            default:
                day = "Invalid day";              
        }
        System.out.println("The day is "+day);
    } 
}
