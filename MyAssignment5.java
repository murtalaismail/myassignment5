/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

/**
 *
 * @author MURTALA
 */
public class Assignment5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Program to accept marks of a student in 5 subjects & calculate the percentage.
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Enter the marks in subject1");
        int subj1 = input.nextInt();
        System.out.println("Enter the marks in subject2");
        int subj2 = input.nextInt();
        System.out.println("Enter the marks in subject3");
        int subj3 = input.nextInt();
        System.out.println("Enter the marks in subject4");
        int subj4 = input.nextInt();
        System.out.println("Enter the marks in subject5");
        int subj5 = input.nextInt();
        
        int sum = subj1+subj2+subj3+subj4+subj5;
        double percentage = (sum/500.0)*100;
        System.out.println("The percentage is "+percentage);   
    }
}
