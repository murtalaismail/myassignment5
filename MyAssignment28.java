/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment28;

/**
 *
 * @author user
 */
public class Assignment28 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to calculate the simple interest for 3 set of P,r and y.
        java.util.Scanner input = new java.util.Scanner(System.in);
        
        for(int i = 1; i <=3; i++){
            System.out.print("Enter the principal ammout: ");
            double p = input.nextDouble();
            if(p==0)
                System.exit(0);
            System.out.print("Enter the rate: ");
            int r = input.nextInt();
            System.out.print("Enter the year: ");         
            int y = input.nextInt();
    
            double interest = (p*r*y)/100;
            System.out.println("The simple interest after "+y+" year(s) is: "+interest);
        }
    }
}
