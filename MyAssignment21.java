/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment21;

/**
 *
 * @author user
 */
public class Assignment21 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Program to accept marks of a student in 5 subjects, 
        calculate the percentage and find his grade using switch case.*/
        
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Enter the marks in subject1");
        int subj1 = input.nextInt();
        System.out.println("Enter the marks in subject2");
        int subj2 = input.nextInt();
        System.out.println("Enter the marks in subject3");
        int subj3 = input.nextInt();
        System.out.println("Enter the marks in subject4");
        int subj4 = input.nextInt();
        System.out.println("Enter the marks in subject5");
        int subj5 = input.nextInt();
        
        int sum = subj1+subj2+subj3+subj4+subj5;
        double percentage = (sum/500.0)*100;
        System.out.println("The percentage is "+percentage);
        
        int intPercentage = (int)(percentage/10);
        char grade;
        switch(intPercentage){
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                 if(percentage <45){
                     grade = 'F';
                 }
                 else{
                     grade = 'D';
                 }
                 break;
            case 5:
                grade = 'C'; break;
            case 6:
                grade = 'B'; break;
            case 7:
            case 8:
            case 9:
            case 10:
                grade = 'A'; break;
            default:
                grade = '0';     
        }
        System.out.println("The student percentage grade is: "+grade);   
    }
}
