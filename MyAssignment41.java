/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment41;

/**
 *
 * @author user
 */
public class Assignment41 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to generate triangle using digits.
        for(int i=1; i<=5; i++){
            for(int j=1; j<=i; j++){
                System.out.print(i+" ");
            }
            System.out.println("");
        }
    }    
}
