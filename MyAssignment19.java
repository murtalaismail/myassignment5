/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment19;

/**
 *
 * @author user
 */
public class Assignment19 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to check whether a given number is even or odd using Conditional Operators.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter a number to check if is prime or odd: ");
        int number = sc.nextInt();
        String status = (number%2==0)?"The number "+number+" is even.":"The number "+number+" is odd.";
        System.out.println(status);
    }    
}